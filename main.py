import sys
from sys import argv


class JustPrinter:
    _words = []
    _line_len = 0
    _width = 0

    def set_width(self, val: int):
        if val < 1:
            raise ValueError
        self._width = val

    def get_width(self):
        return self._width

    def clear(self):
        self._words.clear()
        self._line_len = 0

    def add_word(self, word: str):
        stripped_word = word.strip()
        if self._line_len + len(stripped_word) > self._width:
            if len(self._words) == 1:
                self.print_line(left_justify=True)
            else:
                self.print_line()
        self._words.append(stripped_word)
        self._line_len += len(stripped_word) + 1

    def new_paragraph(self):
        self.print_line(left_justify=True, new_paragraph=True)

    def print_line(
        self, left_justify: bool = False, new_paragraph: bool = False
    ):
        out = ""
        if len(self._words) == 0:
            return
        if left_justify:
            for word in self._words:
                out += word + " "
            out = out[:-1]
        else:
            word_len = 0
            for word in self._words:
                word_len += len(word)
            spaces = self._width - word_len
            partiton_count = len(self._words) - 1
            small_space = spaces // partiton_count
            big_space = spaces // partiton_count + 1
            partitions = [small_space for i in range(partiton_count)]
            for i in range(spaces - small_space * partiton_count):
                partitions[i] = big_space
            for i in range(partiton_count):
                out += self._words[i]
                out += partitions[i] * " "
            out += self._words[-1]
        print(out)
        if new_paragraph:
            print()
        self.clear()


class InputReader:
    def get_line(self):
        return [i for i in input().split() if i]


def error_out():
    print("Error")
    sys.exit(1)


def main():
    printer = JustPrinter()
    try:
        if len(argv) < 2 or int(argv[1]) < 1:
            error_out()
        else:
            printer.set_width(int(argv[1]))
    except ValueError:
        error_out()
    reader = InputReader()
    while True:
        try:
            words = reader.get_line()
        except EOFError:
            printer.print_line(left_justify=True)
            break
        if len(words) == 0:
            printer.new_paragraph()
        else:
            for word in words:
                printer.add_word(word)


if __name__ == "__main__":
    main()
